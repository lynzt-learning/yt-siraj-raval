# try 1
# import input_data
# mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

# try 2
# from tensorflow.examples.tutorials.mnist import input_data
# mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

# try 3
import tensorflow as tf

from official.mnist import dataset
mnist = dataset.download("MNIST_data" )


# import tensorflow as tf

# set params
learning_rate = 0.01
training_iteration = 30
batch_size = 100
display_step = 2
